==========
pyoadr-ven
==========


.. image:: https://img.shields.io/pypi/v/pyoadr_ven.svg
        :target: https://pypi.python.org/pypi/pyoadr_ven


.. image:: https://gitlab.com/carboncoop/pyoadr-ven/badges/master/coverage.svg
        :target: https://gitlab.com/carboncoop/pyoadr-ven/



OpenADR Virtual End Node

* Free software: Apache Software License 2.0
* Module Documentation: https://carboncoop.gitlab.io/pyoadr-ven/
* Project Documentation: https://carboncoop.gitlab.io/opendsr/docs/


OpenADR (Automated Demand Response) is a standard for alerting and responding
to the need to adjust electric power consumption in response to fluctuations
in grid demand.

OpenADR communications are conducted between Virtual Top Nodes (VTNs) and Virtual End Nodes (VENs).
In this implementation, a this agent is a VEN, implementing EiEvent and EiReport services
in conformance with a subset of the OpenADR 2.0b specification.


Credits
-------
* Carbon Co-op
* Battelle Memorial Institute
