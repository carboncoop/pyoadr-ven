.. highlight:: shell

============
Installation
============


Stable release
--------------

To install pyoadr-ven, run this command in your terminal:

.. code-block:: console

    $ pip install pyoadr_ven

This is the preferred method to install pyoadr-ven, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for pyoadr-ven can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git@gitlab.com:carboncoop/pyoadr-ven.git

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.com/carboncoop/pyoadr-ven
.. _tarball: https://gitlab.com/carboncoop/pyoadr_ven/tarball/master
