PyOADR-VEN
==========
Python OpenADR Client Implementation
--------------------------------------


.. toctree::
   :maxdepth: 1

   background
   installation
   usage
   development
   contributing
   history

.. include:: ../README.rst

OpenADR Server Implementation
-----------------------------

This project has been developed alongside a python implementation of the OpenADR VTN client.

This client is available as a `Django application`_, complete with it's own `documentation`_ site.



Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _Django application: https://gitlab.com/carboncoop/pyoadr_vtn/
.. _documentation: https://carboncoop.gitlab.io/pyoadr-vtn/
