.. highlight:: shell

============
Contributing
============

This project began as a part of the `Voltron`_ Kisensum project, developed at the Pacific Northwest National Library.

`Carbon Coop`_ adopted the code in 2019 and continued development as part of the `OpenDSR`_ project.


Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

Building a complete open source implementation of the OpenADR protocol is complex, and we are interested to collaborate with other organisations with a similar aim.

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at https://gitlab.com/carboncoop/pyoadr-ven/issues.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

Fix Bugs
~~~~~~~~

Look through the Gitlab issues for bugs.

Implement Features
~~~~~~~~~~~~~~~~~~

Look through the Gitlab issues for features.

Write Documentation
~~~~~~~~~~~~~~~~~~~

pyoadr-ven could always use more documentation, whether as part of the
official pyoadr-ven docs, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an issue at https://gitlab.com/carboncoop/pyoadr-ven/issues.

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.

.. _Voltron: https://volttron.readthedocs.io/en/develop/core_services/openadr/index.html
.. _Carbon Coop: https://carbon.coop
.. _OpenDSR: https://carbon.coop/opendsr
