import logging
import time
from datetime import datetime
from datetime import timedelta

import coloredlogs
from pony import orm

from pyoadr_ven import database
from pyoadr_ven import OpenADRVenAgent
from pyoadr_ven.enums import ReportStatus
from tests.factories import ReportFactory

coloredlogs.DEFAULT_LOG_FORMAT = "%(asctime)s %(name)s %(levelname)s %(message)s"
coloredlogs.install(level=logging.DEBUG)

logging.debug("LETS GO")

TELEMETRY_PARAMS = {
    "state": {
        "r_id": "evse state",
        "units": "NA",
        "min_frequency": 30,
        "max_frequency": 30,
        "report_type": "",
        "reading_type": "",
        "method_name": "state",
    },
    "amp": {
        "r_id": "evse charging current",
        "units": "A",
        "min_frequency": 30,
        "max_frequency": 30,
        "report_type": "",
        "reading_type": "",
        "method_name": "amp",
    },
    "wh": {
        "r_id": "evse energy used in session",
        "units": "Wh",
        "min_frequency": 30,
        "max_frequency": 30,
        "report_type": "",
        "reading_type": "",
        "method_name": "wh",
    },
}

REPORT_PARAMS = {
    "ccoop_telemetry_evse_status": {
        "report_name_metadata": "ccoop_telemetry_evse_status",
        "report_interval_secs_default": 30,
        "telemetry_parameters": TELEMETRY_PARAMS,
    }
}
VTN_ADDRESS = "https://openadr-staging.carbon.coop"
VEN_ID = "b6364ec4-3a08-46a5-a0ef-9ef7a532f69f"
VTN_ID = "ccoopvtn01"

CERT_PATH = (
    "/home/peter/carboncoop/hems/docker/data/carboncoop-hems-shared-data/client.pem"
)
CA_PATH = "/home/peter/carboncoop/hems/docker/data/carboncoop-hems-shared-data/ca.crt"
DB_PATH = None
logging.info("SETUP DB")
agent = OpenADRVenAgent(
    ven_id=VEN_ID,
    vtn_id=VTN_ID,
    vtn_address=VTN_ADDRESS,
    security_level="standard",
    poll_interval_secs=5,
    log_xml=True,
    opt_timeout_secs=30,
    opt_default_decision="optIn",
    report_parameters=REPORT_PARAMS,
    client_pem_bundle=CERT_PATH,
    vtn_ca_cert=CA_PATH,
)

agent.request_events()
while True:
    agent.tick()
    time.sleep(10)
    logging.info("EVENTS:::")
    for event in agent._active_or_pending_events:
        logging.info(str(event))
